function getServerUrl() {
	//return 'http://10.10.2.175/';
	return 'https://cxdemo.ameyo.com:8786/app_customerPortal/public/index.php/';
}

function getServerUrlDjango() {
	//return 'http://10.10.2.175/';
	return 'http://10.10.2.238:8001/';
}

function refreshTime() {
	return 2000;
}

function setTicketColor(messageType){
	var bgColor = "";
	if(messageType === 'INCOMING_CUSTOMER_PORTAL'){
		bgColor = '#3e2962'
	}else if(messageType === 'INTERNAL_NOTE'){
		bgColor = '#990620' 
	}else if(messageType === 'ACTIVITY'){
		bgColor = '#ccc'
	}else if(messageType === 'EXTERNAL_NOTE'){
		bgColor = '#990620'
	}
	return bgColor
}

function pageRoute(routePath){
	return '/'+routePath
}
