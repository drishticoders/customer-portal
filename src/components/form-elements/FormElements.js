import {Input} from 'react-materialize'
import React from 'react'
const renderTextField = ({input,placeHolder, readonly,onKeyUp}) => {	
	 if(readonly==="true"){
	 	return(<input type="text" placeholder={placeHolder} readOnly {...input}/>

		)
	}
	 return(<input type="text" placeholder={placeHolder}  {...input} onKeyUp={onKeyUp}/>

	)
	
}

const renderPasswordField = ({input,placeHolder}) => {

	 return(<input type="password" placeholder={placeHolder}  {...input} />

	)
	
}




const renderSelect = ({input,className,s,l,m,children}) => (
	<Input type='select' s={s} m={m} l={l} className={className} children={children}  {...input} onChange={input.onChange}/>
	)


const renderTextArea = ({input, className,cols,rows}) => (
<textarea style={{height:"250px"}} rows={rows} cols={cols}  className={className} {...input} ></textarea>
			  	)
const renderCheckBox = ({input, className,label,id}) => (
		<Input type="checkbox" id={id} className={className} defaultChecked={input.value===true?'checked':''} label={label} onChange={input.onChange}/>
	)

module.exports = {
	renderTextField,
	renderPasswordField,
	renderSelect,
	renderTextArea,
	renderCheckBox
	
}