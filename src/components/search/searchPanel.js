import React ,{Component}from 'react'
import { Field, reduxForm,change } from 'redux-form'
import { connect } from 'react-redux'
import {renderTextField,renderSelect} from '../form-elements/FormElements'
import {doSearch} from '../../actions/interationActions'
import {reset} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {DatePicker} from 'material-ui';
class SearchPanel extends Component {
  render(){
         return(            
                <SearchForm onChange={this.props.statusChanged}  enableReinitialize={true} clearData={this.props.clearData} currentSelectedTab={this.props.currentSelectedTab}/>            
    );  
  } 
}

let SearchForm=props => {  
  const{initialValues,statusChanged,clearData, enableReinitialize,currentSelectedTab}=props;  
  return(
  <form>
        <div className="row">
    <div className="col s12 m10 offset-m1 no-padding">
        <div className="row">
            <div className="col s3 m3 search">
                <div className="input-field input-box-style no-margin">                    
                    <Field component={renderTextField} type="text" id="search" name="search" placeHolder="Search"/>
                </div>
                <button type="button" onClick={clearData} className="btn btn-transparent"><i className="mdi mdi-close"></i></button>                
            </div>
            <div style={{display: currentSelectedTab === "recent" ? 'block' : 'none' }} className="col s2 m2 no-padding">
                <div className="input-field input-box-style no-margin no-margin-top">
                    <Field name="status" component={renderSelect} s={12} l={12} m={12}>
                        <option value="">All</option>
                        <option value="New">New</option>
                        <option value="Open">Open</option>
                        <option value="Pending">Pending</option>
                        <option value="Closed">Closed</option>
                    </Field>
                </div>
            </div>
        </div>
    </div>
</div>
    </form>

  
   )
}
SearchForm= reduxForm({
  form: 'SearchPanel'                 // <------ same form name
  
})(SearchForm)
const mapStateToProps=(state) =>{  
  return{
    
  }
}
const mapDispatchToProps=(dispatch,props)=>{
  var searchParam=(values)=>{
        dispatch(doSearch(values));
  }
  var clearFormData = (values)=>{
     dispatch(change('SearchPanel','search',''));
  }
   return {    
    statusChanged : searchParam,
    clearData : clearFormData
  }
}
 SearchPanel = connect(mapStateToProps,mapDispatchToProps)(SearchPanel)

 export default SearchPanel

