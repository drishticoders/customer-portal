import React ,{Component}from 'react'
import { Field, reduxForm } from 'redux-form'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import {renderTextArea,renderTextField} from '../form-elements/FormElements'
import {addInteraction} from '../../actions/interationActions'

let RaiseTicket = ({onSaveInteraction}) => {
   return (
          
           <CreateTicket handleSubmit={onSaveInteraction} />
  ) 
}

class CreateTicket extends Component {
  render(){
         return(
                              
               <div className="row mb15 text-area-wrapper-main  show-text-area">
                  <CreateTicketForm onSubmit={this.props.handleSubmit} />
                </div>
            
    );  
  } 
}

let CreateTicketForm=props => {
  const{handleSubmit}=props;

    return(
  
    <div id="modal1" className="modal">
    <form onSubmit={handleSubmit}>
   <div className="modal-header row">
      <div className="col s12 m12 l12">
         <h4 className="left">Raise Ticket</h4>
         <a href="#!" className="modal-action modal-close right"><i className="mdi mdi-close"></i></a>
      </div>
   </div>
   <div className="divider"></div>
   <div className="modal-content row">
      <div className="col s12 m12 l12">
          <div className="row">
               <div className="input-field col s12 l12 m12">
                    <Field component={renderTextField} type="text" className="active" id="Subject" name="subject" />
                    <label htmlFor="Subject">Subject</label>
               </div>
            </div>
            <div className="row mt8">
               <div className="input-field col s12 l12 m12 mb0">
                    <Field component={renderTextArea} type="text" className="materialize-textarea" id="textarea1" name="description" />
                     <label htmlFor="" className="active">Message</label>
               </div>
            </div>

            <div className="row mt8">
               <div className="input-field col s12 l12 m12 mb0">
                    <Field component={renderTextArea} type="text" className="materialize-textarea" id="textarea2" name="comment" />
                     <label htmlFor="" className="active">Comment</label>
               </div>
            </div>
          
      </div>
   </div>
   <div className="divider"></div>
   <div className="modal-footer row">
      <div className="col s12 m12 l12 multiple-btn">
          <button type="submit"  className="modal-action modal-close waves-effect btn btn-primary">Raise Ticket</button>
          <a href="#!" className="modal-action modal-close waves-effect btn btn-secondary">Cancel</a>
      </div>
   </div>
    </form>
</div>
  
  
   )
}

CreateTicketForm= reduxForm({
  form: 'AddInteractionForm'
  
})(CreateTicketForm)

const mapStateToProps=(state) => 
(
  
  {
   
  }
)

const mapDispatchToProps=(dispatch,props)=>{
   var createTicket = (values) => {
      dispatch(addInteraction(values))
  }
  return {
    onSaveInteraction : createTicket
   
   }
  
}

 RaiseTicket = connect(mapStateToProps,mapDispatchToProps)(RaiseTicket)

 export default RaiseTicket

