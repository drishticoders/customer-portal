import React ,{Component}from 'react'
import { Field, reduxForm } from 'redux-form'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import {renderTextArea} from '../form-elements/FormElements'
import {addNoteForInteraction} from '../../actions/interationActions'

 /* global $ */

 let NotePage = ({onSaveNote, noteAddData}) => {
  return (
    <Note handleSubmit={onSaveNote} noteAddData={noteAddData}/>
  )
}
const openModal=()=>{
  $(".show-text-area").slideToggle();  
}
class Note extends Component {
  render(){
    return(              
      <div className="row mb15 text-area-wrapper-main  show-text-area">
        <NoteForm onSubmit={this.props.handleSubmit} noteAddData={this.props.noteAddData}/>
      </div>            
    );  
  } 
}

let NoteForm=props => {
  const{handleSubmit, noteAddData}=props;
  return(
  <form onSubmit={handleSubmit} >
    <div className="col s12 m12 l12 text-area-wrapper">
      <div className="row">
        <div className="col s12 m12 l12 relative">
          <Field component={renderTextArea} type="text" id="note" name="note" />
        </div>
      </div>
      <div className="row">
        <div className="col s12 m12 l12 footer-btn multiple-btn right-align"> 
          <a className="waves-effect waves-light btn  footer-btn-secondary" onClick={(e)=>{openModal()}}>Cancel</a> 
          <button type="submit" disabled={!noteAddData}  className="waves-effect waves-light btn footer-btn-primary" onClick={(e)=>{openModal()}}>Add</button>
        </div>
      </div>
    </div>
  </form>
  )
}

NoteForm= reduxForm({
  form: 'AddNoteForm'
  
})(NoteForm)
const mapStateToProps=(state) => ({
  noteAddData : (state.form.AddNoteForm === undefined || state.form.AddNoteForm.values === undefined) ? '' : state.form.AddNoteForm.values.note
})

const mapDispatchToProps=(dispatch,props)=>{
  var addNote = (values) => {
    dispatch(addNoteForInteraction(values))
  }
  return {
    onSaveNote : addNote   
  }  
}

NotePage = connect(mapStateToProps,mapDispatchToProps)(NotePage)
export default NotePage

