import React ,{Component}from 'react'
import { Field, reduxForm } from 'redux-form'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import { Link } from 'react-router'
import NotePage from './AddNote'
import Loading from '../Loading'

 /* global $ */

 const MessageRow=({message,bgColor})=>{            
          return(
             <div className="row single-infomation mb15">
                <div className="col s12 m12 l12">
                  <div className="row mb8">
                    <div className="col s12 m12 l12 no-padding">
                      <div className="row">
                        <div className="col s12 m12 l12 no-padding">
                          <h5 className="label-text bold left"><div dangerouslySetInnerHTML={{ __html:  message.textMessage }} /></h5>
                          <span className="label-info remaining-info date-time-info left">{new Date(message.dateAdded).toLocaleString()}</span>
                         </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                <i style={{backgroundColor: bgColor}} className="show-status status-success"></i>
              </div>
             );
  }

const openModal=()=>{
     $(".show-text-area").slideToggle();
  
}
const InterationDetail = (interactionData) => {  
 var myIntereration = interactionData.interactionData.data;
 var customerId = interactionData.customerId;
 var campaignId = myIntereration.campaignId;
 var messageRow = [];
 var bgColor = "";
 if(myIntereration && myIntereration.message.messages){
        Object.keys(myIntereration.message.messages).forEach((key) => {
           var i = 'row-'+key+'-'+myIntereration.message.messages[key].messageId;
           bgColor = window.setTicketColor(myIntereration.message.messages[key].messageType)           
           messageRow.push(<MessageRow message={myIntereration.message.messages[key]} key= {i} bgColor= {bgColor}/>);
        });
   }   
   if(myIntereration.message === ""){
    return (
      <Loading/>
      )
    }else{
   return (    
    <main className="">
      <div className="row ticket-top-info">
        <div className="col s12 m8 l7 offset-l1 ">
          <div className="row mb8">
            <div className="col s12 m12 l12 bold-text">
              <h3 className="left label-text">Ticket Reference Number : </h3>
              <span className="left label-info">{myIntereration.ticketId}</span> </div>
          </div>
          <div className="row mb8">
            <div className="col s12 m12 l12"> <span className="left label-text">Subject :</span> <span className="left label-info">{myIntereration.subject}</span> </div>
          </div>
          <div className="row mb8">
            <div className="col s12 m12 l12"> <span className="left label-text">Status :</span> <span className="left label-info">{myIntereration.externalState}</span> </div>
          </div>
          <div className="row mb8">
            <div className="col s12 m12 l12"> <span className="left label-text">Last Updated On :</span> <span className="left label-info">{new Date(myIntereration.dateModified).toLocaleString()}</span> </div>
          </div>
        </div>
        <div className="col s12 m4 l3 offset-sl1  right-align"><Link to={'/ticket_list/'+customerId} className="waves-effect waves-light back-ticket-btn">Back to home</Link></div>
      </div>    
      <div className="row ticket-bottom-info">
        <div className="col s12 m12 l10  offset-l1">
          <div className="bottom-info-wrapper row ">
            <div className="col s12 m12 l12 relative">
              <div className="row mb15">            
                <div className="col s12 m6 l6 multiple-btn"></div>
                <div className="col s12 m6 l6 multiple-btn right-align"> 
                    <a className="waves-effect waves-light btn  btn-primary" id="add-comment" onClick={(e)=>{openModal()}}>Add Comment</a>
                </div>            
              </div>          
              <NotePage/>
              <div className="row">
                <div className="col s7 m7 l7 no-padding manage-ticket-scroll">
                  <div className="row single-infomation mb15">
                    <div className="col s12 m12 l12">
                      <div className="row mb8">
                        <div className="col s12 m12 l12 no-padding">
                          <h5 className="left label-text bold">Created On : </h5>
                          <span className="left label-info bold">{new Date(myIntereration.creationDate).toLocaleString()}</span> </div>
                      </div>
                    </div>
                    <i className="show-status status-success"></i> </div>
                      {messageRow}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  
 )} 
}

export default InterationDetail