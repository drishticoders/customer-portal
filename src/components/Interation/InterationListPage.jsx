import React ,{Component}from 'react'
import { Field, reduxForm } from 'redux-form'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import InterationTable from '../../components/Interation/InterationTable'
import RaiseTicketButton from '../../components/Interation/RaiseTicketButton'
import RaiseTicket from '../../components/Interation/createInteraction'
import SearchPanel from '../../components/search/searchPanel'
import {getAllInterationForCustomer,selectedRows,setPageNumber} from '../../actions/interationActions'
 /* global $ */

let InterationListPage = ({interationList,showRecent,currentSelectedTab,
                          setInterationId,setPageNumber,currentPage,
                          defaultTab, ticketMessage}) => {     
   return (
    <div>
     <div className="row customer_tab">
      <div className="col s12 l12 m12 no-padding">
        <div className="row tab-border">
          <div className="col s8 offset-s1 ">
            <ul className="tabs">
              <li className="tab col s2"><a href="#Recent" className={currentSelectedTab === "recent"?"active":""} onClick={(e)=>showRecent("recent")}>Recent Ticket</a>
              </li>
              <li className="tab col s2"><a href="#Closed" className={currentSelectedTab === "closed"?"active":""} onClick={(e)=>showRecent("closed")}>Closed Ticket</a>
              </li>
            </ul>
		
          </div>


          <RaiseTicketButton/>          
       </div>
       <SearchPanel currentSelectedTab={currentSelectedTab}/>
      <div className="row">
        <div className="col s10 offset-s1 ">
            <InterationTable 
              interationList = {interationList} 
              currentSelectedTab = {currentSelectedTab} 
              setInterationId ={setInterationId} 
              setPageNumber={setPageNumber} 
              currentPage={currentPage} 
              defaultTab={defaultTab}
              ticketMessage = {ticketMessage}
            />
        </div>
        </div>
      </div>
    </div>
    <RaiseTicket/>
    <footer className="page-footer login-footer">
            <div className="powered-by-logo">
                <img src="/images/ameyoLogo.png"/><span className="powered-text">Powered by Ameyo</span>
            </div>
        </footer>
    </div>
 ) 
}
const mapStateToProps=(state,ownProps) =>
(

  {
    interationList:getAllInterationForCustomer(state),
    currentSelectedTab:(state.uiSelection.tabName === undefined) ? 'recent' : state.uiSelection.tabName,
    currentPage:(state.uiSelection === undefined) ? '0' : state.uiSelection.pageNumber,
    defaultTab:(state.uiSelection === undefined) ? 'recent' : state.uiSelection.defaultTab,
    ticketMessage : state.uiSelection === undefined ? '' : state.uiSelection.ticketDataMessage
  }
)
const mapDispatchToProps=(dispatch,props)=>{
   return {    
    showRecent: (values) => dispatch({type:"SET_TAB",tabName:values}),
    setInterationId: (values,campaignId, goBack) =>dispatch(selectedRows(values,campaignId, goBack)),
    setPageNumber: (values) => dispatch(setPageNumber(values))

    //showClosed: () => dispatch({type:"SET_TAB",tabName:"closed"}) 
  }
}
 InterationListPage = connect(mapStateToProps,mapDispatchToProps)(InterationListPage)

 export default InterationListPage

