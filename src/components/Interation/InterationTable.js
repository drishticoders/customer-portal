import React from 'react'
import ReactTable from 'react-table'
import "react-table/react-table.css";
import Time from 'react-time-format'
import matchSorter from 'match-sorter'

const InterationTable = ({interationList,currentSelectedTab,redirecToDetail,setInterationId,setPageNumber,currentPage,defaultTab}) => {        
    var interactionData = "";    
    if (currentSelectedTab === 'recent') {
        interactionData = interationList.data.recentTicket
    } else {
        interactionData = interationList.data.closedTicket
    }
    var interactionListData = [];
    for (var item in interactionData) {
        interactionListData.push(interactionData[item]);
    }
    var rows = [];
    const data = interactionListData

    function onRowClick(props) {
        var ticketID =props.ticketId;
        var campaignId =props.campaignId;
        setInterationId(ticketID, campaignId)        
    }

    function getPageNumber(pageNumber){        
        setPageNumber(pageNumber)        
    }

    const columns = [{
        Header: 'Reference No.',
        accessor: 'ticketId',
        id: "ticketId",        
        Cell: props => <a href="#" onClick={()=>{onRowClick(props.original);}} className="data-link">{props.value}</a>
    }, {
        Header: 'Subject',
        id: "subject",
        accessor: d => d.subject        
    }, {
        Header: 'Status',
        accessor: 'externalState',
        id: "externalState"
        
    }, {
        Header: 'Update On',
        accessor: 'dateModified',
        id: "dateModified",
        Cell: props => <a>{new Date(props.original.dateModified).toLocaleString()}</a>       

    }]
    return ( <
        ReactTable data = {
            data
        }
        expander= {true}
        loadingText={'Loading...'}
        //page= {currentPage}
        //onPageChange= {getPageNumber}
        sortable={false}        
        nextText={'>'}
        previousText={'<'}        
        noDataText = "Great you don't have any ticket.."
        columns = {
            columns
        }
        defaultPageSize = {9}

        />

    );
}

export default InterationTable
