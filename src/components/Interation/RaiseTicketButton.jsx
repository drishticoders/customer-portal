import React ,{Component}from 'react'

 /* global $ */

 const openModal=()=>{
  $('.modal').openModal();
}


let RaiseTicketButton = ({}) => {
   return (
             <div className="col s3">
                <a className="waves-effect waves-light btn modal-trigger mt2 btn-primary raise-issue" href="#modal1" onClick={(e)=>{openModal()}}>Raise Ticket
          		</a>
              </div>
  ) 
}

export default RaiseTicketButton