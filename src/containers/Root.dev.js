import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import DevTools from './DevTools'
import MainContainer from '../containers/MainContainer'
import CustomerPortalLogin from '../containers/Login/CustomerPortalLoginContainer'
import TicketList from '../containers/TicketList/TicketListContainer'
import InterationDetail from '../containers/InterationDetails/InterationDetailContainer'
import loading from '../components/loading'
import { Router, Route,IndexRoute } from 'react-router'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
const lightMuiTheme = getMuiTheme(
      {flatButton: { primaryTextColor: '#EF5350' }},
       {datePicker: {
        selectColor: '#EF5350',
        selectTextColor: '#FFF',
        headerColor: '#EF5350'
    }}
  );

const Root = ({ store, history }) => (

  <Provider store={store}>
  <MuiThemeProvider muiTheme={lightMuiTheme}> 
  <div>
     <Router history={history}>
      <Route path="/portal" component={MainContainer}>
        <IndexRoute component={CustomerPortalLogin} />
          <Route path="login" component={CustomerPortalLogin}/>
          <Route path="loading_screen" component={loading}/>
          <Route path="ticket_list/:customerId" component={TicketList}/>
          <Route path="interaction_detail/:customerId/:campaignId/:interactionId" component={InterationDetail}/>
        </Route>
    </Router>

   </div>
   </MuiThemeProvider>
  </Provider>
 )

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}

export default Root
