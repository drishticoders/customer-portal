import React, {PureComponent} from 'react'
import {getInterationById} from '../../actions/interationActions'
import {connect} from 'react-redux'
import InterationDetail from '../../components/Interation/InteractionDetail'
import {refreshInteractionDetail,refreshTicket} from '../../actions/interationActions'

class InterationDetails extends PureComponent {
    constructor(props) {
        super(props)
    }

    componentDidMount() {            
        var refreshInterval = window.refreshTime();         
        this.ticketDetailTimer = setInterval(this.props.onRefreshTicket, refreshInterval);
        this.ticketTimer = setInterval(this.props.onRefreshInteraction, refreshInterval);        
    }
    componentWillUnmount() {
        window.clearInterval(this.ticketDetailTimer);
        window.clearInterval(this.ticketTimer);
    }
    render() {
        return (
            <InterationDetail 
                interactionData={this.props.interactionData} 
                customerId={this.props.customerId}
            />
        )
    }
}

const mapStateToProps = (state, ownProps) =>{  
    var customerId = ownProps.params.customerId    
    return{        
          customerId: customerId,
          interactionData:getInterationById(state)
        }
    }    

const mapDispatchToProps = (dispatch, ownProps) => {    
    var campaignId    = ownProps.params.campaignId
    var interactionId = ownProps.params.interactionId    
    var customerId = ownProps.params.customerId === undefined?'blank':ownProps.params.customerId
    return {
        onRefreshInteraction: () => dispatch(refreshTicket(customerId)),
        onRefreshTicket: () => dispatch(refreshInteractionDetail(campaignId,interactionId))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(InterationDetails)