import React, { Component } from 'react'
import NotificationWidget from '../components/NotificationWidget'
import LogoutPage from '../components/logout/logout'
import { Cookies } from 'react-cookie';
import Header from './Header'

const cookies = new Cookies();
class MainContainer extends Component {
  constructor(props) {
    super(props) 
  }

  render() {
        return (
                <div>                 
                  <Header/>
                  <main className="login-section">
                    {this.props.children} 
                    {cookies.get('userSessionData')!==undefined && cookies.get('userSessionData')!=='' ? (<LogoutPage/>):("")}                    
                   </main>
                 <NotificationWidget/>
                  </div>
               )
  }
}

export default MainContainer
