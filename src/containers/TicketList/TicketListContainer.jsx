import React, {Component} from 'react'
import {connect} from 'react-redux'
import InterationListPage from '../../components/Interation/InterationListPage'
import {refreshTicket} from '../../actions/interationActions'
 /* global $ */
class TicketList extends Component {
    componentDidMount() {          
        var time = window.refreshTime();
        //this.timer = setInterval(this.props.onRefreshTicket, time);
        this.props.interactionList.length === 0 ? this.props.onRefreshTicket() : ''
        $('ul.tabs').tabs();
  
        
    }
    componentWillUnmount() {
        window.clearInterval(this.timer);
    }
    render() {
        return (
            <InterationListPage />
        )
    }
}
const mapStateToProps = (state, ownProps) => {         
    return {
        customerId : ownProps.params.customerId,
        interactionList : state.setData.interactionData === undefined ? '' : state.setData.interactionData

    }
}
const mapDispatchToProps = (dispatch, ownProps) => {    
    var customerId = ownProps.params.customerId === undefined?'blank':ownProps.params.customerId
    return {
        onRefreshTicket: () => dispatch(refreshTicket(customerId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketList)