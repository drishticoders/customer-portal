import React, { Component } from 'react'
import LogoutPage from '../components/logout/logout'
import { Link, History } from 'react-router'

class Header extends Component{
  constructor(props) {
    super(props)
   
  }
  render() {
 

   return (
  <header>
            <nav>
                <div className="nav-wrapper">
                    <div className="row">
                        <div className="col s12 m12">
                          
                            <Link to={'/ticket_list/6528'} className="brand-logo">
                            <img src="/images/moneySure.png" alt="MoneySure"/>
                            </Link>
                                  
                        </div>
                    </div>
                </div>
            </nav>
        </header>

    )
    
  }
}

export default Header;
