import React, { Component,Fragment} from 'react'
import {doLogin,checkUserLoginStatus} from '../../actions/loginActions'
import { connect } from 'react-redux'
import LoginPage from '../../components/Login/LoginPanel'
import {Cookies} from 'react-cookie';
import configureStore from '../../store/configureStore'
import {push} from 'react-router-redux'
import {hashHistory} from 'react-router'

const store = configureStore()

const cookies = new Cookies();
class CustomerPortalLogin extends Component {
  constructor(props) {
    super(props) 
 }

  componentDidMount() {    
    // var cstId = this.props.loginCustomerId;    
    // console.log("cstId===",cstId)
    //  if(cstId !== undefined){          
    //    setTimeout(function(){
    //     hashHistory.push('/ticket_list/'+cstId)
    //     //store.dispatch(push('/ticket_list/'+cstId)) 
    //   }, 1000);
    //  }    
  }
 render() {
   return (
    <div className ="login-page">
        <div className="login-section">
        <div className="row login-page">
          <div className="col s12 m8 l5 login-form">
            <h1 className="center-align">Login to the Customer Portal</h1>
            <LoginPage handleSubmit = {this.props.onLogin}/>
           </div>
          </div>
          </div>          
      </div>
      
    )
  }
}
const mapStateToProps = (state, ownProps) => {  
  var cstId = state.login.loginStatus.message === undefined ? '' : state.login.loginStatus.message.customer_id
    return {
      loginCustomerId : cstId
     
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {  
   var loginParam=(values)=>{
        dispatch(doLogin(values));
  }
   var checkLogin = () => {
        dispatch(checkUserLoginStatus())
   }
  return {
   onLogin:loginParam,
   checkLoginStatus: checkLogin
   }
}
export default connect(mapStateToProps,mapDispatchToProps)(CustomerPortalLogin)
