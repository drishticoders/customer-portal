import { reducer as formReducer } from 'redux-form'
import uiSelection from './UISelectionReducer'
import login from './loginReducer'
import logout from './logoutReducer'
import setData from './setDataReducer'
import interationDetails from './interationDetailsReducer'
import {routerReducer } from 'react-router-redux'
import {combineReducers} from 'redux'

export const rootReducer = combineReducers({
 form: formReducer,     // <---- Mounted at 'form'
 routing: routerReducer,
 uiSelection,
 login,
 logout,
 setData,
 interationDetails
 });
export default rootReducer


