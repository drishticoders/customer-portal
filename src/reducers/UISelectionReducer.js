const uiSelection=(state={},action) =>{
switch(action.type){
	case 'SET_KB_TEXT':
	var entityVal=undefined;
	   entityVal={
			userSearch:action.inputText,
			
		}
	return {
		...state,
		userSearchText:action.inputText
	}
	case 'SET_ALL_USERS':
	return {
		...state,
		usersList:action.data
	}
	case  'SET_SELECTED_USER':
	return {
		...state,
		selectedUserToDial:action.data
	}
	case  'SET_TAB':		
	return {
		...state,
		tabName:action.tabName
	}	
	case 'SET_INTERATION_ID':
	return{
		...state,
		interactionId:action.interactionId
	}
	case 'FILTER_PARAM':
	return{
		...state,
		filterKey:action.data
	}
	case 'PAGE_NUMBER':	
	return {
		...state,
		pageNumber:action.pageNumber
	}
	case 'DEFAULT_TAB':	
	return {
		...state,
		defaultTab:action.defaultTab
	}
	case 'TICKET_DATA_MESSAGE':
	return {
		...state,
		ticketDataMessage : action.interactionMessage
	}
	default:
		return state;
  }
}
export default uiSelection