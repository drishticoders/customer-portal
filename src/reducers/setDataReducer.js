const setData=(state={interactionData:[]},action) =>{
switch(action.type){
	case 'TICKET_DATA':
	return {
		...state,
		interactionData : action.interactionData		
	}	
	case 'SET_SELECTED_INTERACTION_DATA':
		return {
			...state,
			interactionId:action.interactionId,
			campaignId:action.campaignId
		}
	case 'GO_BACK':
		return{
			...state,
			goBack:action.goBack
		}
	default:
		return state;
  }
}
export default setData