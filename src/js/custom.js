$(document).ready(function(){
             $(".button-collapse").sideNav();
			 /*right sidebar height*/
			 var window_height = $(window).height();
			 var header_height = $(".navbar-fixed").height();
			 var main_panel = window_height - header_height;
			 
			// alert(window_height);
			//  alert($(".navbar-fixed").height());
			// alert(right_sidebar_hgt);
			$(".sidebar").height(main_panel);
			$(".login-wrapper").height(window_height); 
	
			//drop down code
			$('select').material_select();
			
	
	/*Chart js start here*/
	// JavaScript Document
$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
			allowDecimals: false,
            categories: ['Agent']			
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Agents',
               
            }      
        },
        legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Unavailable',
            data: [10 ] 
        }, {
            name: 'Available',
            data: [5]        
        }]
    });
});

$(function () {
    Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $(document).ready(function () {

        // Build the chart
        $('#containerPie').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
          
			legend: {
                 layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle',
              borderWidth: 0,
              itemMarginTop: 1,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
                 },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Status',
                colorByPoint: true,
                data: [{
                    name: 'New ',
                    y: 312
                }, {
                    name: 'Open',
                    y: 104,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Closed ',
                    y: 13                
                },
				{
                    name: 'Expired ',
                    y: 100
                },
				{
                    name: 'Merged ',
                    y: 20
                },
				
				{
                    name: 'Splitted ',
                    y: 10
                },
				{
                    name: 'Pending ',
                    y: 23
                }
				
				]
            }]
        });
    });
});


$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#containerSla').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              hour: '%H:%M',
			  day: '%A:%b',
			  week: '%e:%b',
            }
         
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
            headerFormat: '{point.x:%A , %b %e , %H:%M}<br>',
            pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Assigned',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'First Response',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Resolve',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        }]
    });
});


/*line chart*/
$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
	 $('#containerPeriod').highcharts({
  		
        title: {
            text: ''
        },
       
        xAxis: {
            categories: ['0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', '10', '11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
        },
        yAxis: {
            title: {
                 text: 'Number of Interaction'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
         
        },
       
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
		},
        series: [{
           name: 'Received',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
           name: 'Resolved',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
});
/*line chart*/

/*overview graph*/
$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#assignedSla_first').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'First Assignded SLA'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              hour: '%H:%M',
			  day: '%A:%b',
			  week: '%e:%b',
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
           
             pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});

$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#responseSla_first').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'First Response SLA'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              hour: '%H:%M',
			  day: '%A:%b',
			  week: '%e:%b',
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
           
           pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

       series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});


$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#resolveSla_first').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'First Resolve SLA'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                hour: '%I %p',
        		minute: '%I:%M %p'
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
             pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

         series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});


/*overview graph*/
$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#assignedSla_second').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                hour: '%I %p',
        		minute: '%I:%M %p'
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
              pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

         series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});

$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#responseSla_second').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                hour: '%I %p',
        		minute: '%I:%M %p'
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
          
              pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

         series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});


$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#resolveSla_second').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                hour: '%I %p',
        		minute: '%I:%M %p'
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
              pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

         series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});


/*Chart msg data*/
$(function () {
 Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $(document).ready(function () {

        // Build the chart
        $('#container_msg_data').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
            },
			legend: {
                 layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle',
              borderWidth: 0,
              itemMarginTop: 1,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
                 },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Data',
                colorByPoint: true,
                data: [{
                    name: 'Voice ',
                    y: 56
                }, {
                    name: 'Non Voice',
                    y: 24,
                    sliced: true,
                    selected: true
                }]
            }]
        });
    });
});

/*Chart interaction data*/
$(function () {
  Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $(document).ready(function () {

        // Build the chart
        $('#container_iteraction_data').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                 pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
            },
			legend: {
                 layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle',
              borderWidth: 0,
              itemMarginTop: 1,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
                 },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Data',
                colorByPoint: true,
                data: [{
                    name: 'Inbound Call ',
                    y: 56
                }, {
                    name: 'Outbound Call',
                    y: 24,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Incoming Mail',
                    y: 10
                }, {
                    name: 'Outgoing Mail',
                    y: 4                
                }]
            }]
        });
    });
});



//chat history graph
$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#first').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'First Response Time'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              hour: '%H:%M',
			  day: '%A:%b',
			  week: '%e:%b',
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
           
             pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});


$(function () {
	Highcharts.setOptions({
         		colors: [ '#c1272d', '#40a4dc', '#F39C12','#22ae5f', '#f7931e','#1072ba','#808080']
    });
    $('#second').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Inchat Response Time'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              hour: '%H:%M',
			  day: '%A:%b',
			  week: '%e:%b',
            },
            title: {
                text: 'Time'
            }
        },
        yAxis: {
            title: {
                text: 'SLA Percentage'
            },
            min: 0
        },
		legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0,
              itemMarginTop:0,
              itemStyle: {
                fontWeight: 'normal',
                fontSize: '12px'
           }
        },
        tooltip: {
           
             pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Booking',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 80], 
				[Date.UTC(2016, 4, 21, 3, 20), 80], 
				[Date.UTC(2016, 4, 21, 2, 25), 80]
            ]
        }, {
            name: 'Support',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 60], 
				[Date.UTC(2016, 4, 21, 3, 20), 60], 
				[Date.UTC(2016, 4, 21, 2, 25), 60]
            ]
        }, {
            name: 'Feedback',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 40], 
				[Date.UTC(2016, 4, 21, 3, 20), 40], 
				[Date.UTC(2016, 4, 21, 2, 25), 40]
            ]
        },{
            name: 'Interaction campaign',
            data: [
                [Date.UTC(2016, 4, 21, 1, 15), 50], 
				[Date.UTC(2016, 4, 21, 3, 20), 50], 
				[Date.UTC(2016, 4, 21, 2, 25), 50]
            ]
        }]
    });
});



	
	/*Chart js ends here*/
	
      }); 