import { getAllInteration } from './interationActions'
import { push } from 'react-router-redux'
import { Cookies } from 'react-cookie';
import { notify } from '../components/NotificationWidget'
import { hashHistory, browserHistory } from 'react-router'
//import { pushState } from 'redux-router';
const cookies = new Cookies();

export function checkUserLoginStatus() {
    return (dispatch, getState) => {
        var session_id = (cookies.get('session_id') !== undefined) ? cookies.get('session_id') : undefined
        if (session_id !== undefined) {
            // dispatch(push('ticket_list'));
        }
    }
}

function redirect() {
    hashHistory.replace('/portal/')
}

function handleErrors(response) {
    if (!response.ok) {
        notify.createShowQueue()("failure", "Invalid Username or Password");
        setTimeout(function () {
            redirect();
        }, 1000);
        return false;
    }
    return response;
}
export function doLogin(values) {
    return (dispatch, getState) => {
        //dispatch(push('/portal/loading_screen'));
        //dispatch(push('/portal/ticket_list/6528'));
        //var url = window.getServerUrl();
        var url = window.getServerUrlDjango();
        fetch(url + 'login/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            /*body: JSON.stringify({
                username: values.userName,
                password: values.loginPassword,
            })*/
            body: JSON.stringify({
                userName: values.userName,
                password: values.loginPassword,
            })
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json()
            }).then(function (body) {
                if (body.status == 200 && body.message == "Successfully Logged In") {
                    dispatch({
                        type: 'LOGIN_SUCCESS',
                        message: body.loginId
                    })
                    cookies.set('userSessionData', body, {
                        path: '/'
                    });
                    notify.createShowQueue()("success", "Logged In Successfully");
                    hashHistory.replace('/portal/ticket_list/1')
                } else {
                    notify.createShowQueue()("success", "Invalid Username Or Password");
                    window.location.reload()
                }
            });
    }
}

export function doLogout() {
    return (dispatch, getState) => {
        var loginUser = cookies.get('userSessionData');
        cookies.set('userSessionData', '', {
            path: '/'
        });
        //dispatch(push('/portal/loading_screen'));
        //var url = window.getServerUrl();
        var url = window.getServerUrlDjango();
        fetch(url + 'logout/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                logout: 'true',
                loginId: loginUser.loginId
            })
        })
            .then(function (response) {
                return response.json()
            }).then(function (body) {
                if (body.status == 200 && body.message == "Successfully Logged Out") {
                    cookies.set('userSessionData', '', {
                        path: '/'
                    });
                    notify.createShowQueue()("success", "Logged Out Successfully");
                    hashHistory.replace('/portal/')
                } else {
                    notify.createShowQueue()("success", "Could not be logged Out");
                }
            });


    }
}

export function setUserData() {
    //console.log("cookies.get==",cookies.get('userSessionData'))
    return dispatch => {
        dispatch({
            type: 'LOGIN_SUCCESS',
            message: cookies.get('userSessionData')
        })
    }
}
