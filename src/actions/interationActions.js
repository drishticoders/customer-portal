import { push } from 'react-router-redux'
import { notify } from '../components/NotificationWidget'
import { change } from 'redux-form';
import { Cookies } from 'react-cookie';
import { reset } from 'redux-form';
import { hashHistory } from 'react-router'

const cookies = new Cookies();
export const refreshTicket = (customerId) => {
    return (dispatch) => {
        dispatch(getAllInteration(customerId));
    }
}
export const refreshInteractionDetail = (campaignId, interactionId) => {
    return (dispatch, getState) => {
        var state = getState();
        var redirectList = false;
        dispatch(selectedRows(interactionId, campaignId, redirectList));
    }
}
export function selectedRows(interactionId, campaignId, redirectList) {
    return (dispatch, getState) => {
        var state = getState();
        var customerId = state.login.loginStatus.message.customer_id === undefined ? '' : state.login.loginStatus.message.customer_id;

        dispatch({
            type: "SET_SELECTED_INTERACTION_DATA",
            interactionId: interactionId,
            campaignId: campaignId
        });
        var url = window.getServerUrl();
        fetch(url + 'api/getAllMessagesByInteractionId', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                interactionId: interactionId,

            })
        })
            .then(function (response) {
                return response.json();
            }).then(function (interactionMessage) {
                dispatch({
                    type: 'INTERACTION_MESSAGE',
                    message: interactionMessage
                });
                if (redirectList !== false) {
                    hashHistory.push('interaction_detail/' + customerId + '/' + campaignId + '/' + interactionId)
                    //dispatch(push('/portal/interaction_detail/' + customerId + '/' + campaignId + '/' + interactionId));

                }
            });

    }
}
export const getInterationById = (state) => {
    var selectedInteractionData = {}
    var interactionId = state.setData.interactionId;
    var interactionData = state.setData.interactionData;
    if (interactionData !== null) {
        var interactionDataLength = interactionData.length;
        for (var i = 0; i < interactionDataLength; i++) {
            if (interactionData[i].ticketId === interactionId) {
                selectedInteractionData = interactionData[i]
            }
        }
        selectedInteractionData['message'] = (state.interationDetails.interactionMessages === undefined) ? '' : state.interationDetails.interactionMessages;
        return {
            data: selectedInteractionData
        }
    }
}

export const getAllInterationForCustomer = (state, customerId) => {
    var obj = {}
    let interactionData = state.setData.interactionData;
    const acceptedValues = ["Closed"]
    var recentTicket = {};
    var closedTicket = {};
    var i = 0;
    var j = 0;

    function filter(tickets, searchParams) {
        return tickets.filter(function (obj) {
            return Object.keys(searchParams).every(function (c) {
                return obj[c].toLowerCase().includes(searchParams[c].toLowerCase());
            });
        });
    }
    var filterParam = (state.uiSelection.filterKey === undefined) ? '' : state.uiSelection.filterKey;
    var filterCount = Object.keys(filterParam).length;
    if (filterCount > 0) {
        filterParam.search = filterParam.search === undefined ? '' : filterParam.search
        filterParam.status = filterParam.status === undefined ? '' : filterParam.status
        var interactions = filter(interactionData, {
            ticketId: filterParam.search,
            externalState: filterParam.status
        });
    }
    for (let e in interactions) {
        if (interactions.hasOwnProperty(e)) {
            if (acceptedValues.indexOf(interactions[e]['externalState']) !== -1) {
                closedTicket[i] = interactions[e];
                i++;
            } else {
                recentTicket[j] = interactions[e];
                j++;
            }
        }
    }

    obj['recentTicket'] = recentTicket;
    obj['closedTicket'] = closedTicket;
    return {
        data: obj
    }
}
export function addNoteForInteraction(values) {
    return (dispatch, getState) => {
        var state = getState();
        var campaignId = state.setData.campaignId;
        var interactionId = state.setData.interactionId;
        var url = window.getServerUrl();
        fetch(url + 'api/addMessage', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                interactionId: interactionId,
                message: values.note,
                campaignId: campaignId,

            })
        })
            .then(function (response) {
                return response.json();
            }).then(function (addNoteResponse) {
                if (addNoteResponse.interactionId !== undefined) {
                    notify.createShowQueue()("success", "Note Added Successfully");
                } else {
                    notify.createShowQueue()("failure", "Something went wrong while adding note");
                }

            });
        dispatch(reset('AddNoteForm'));
    }
}
export function addInteraction(values) {
    return (dispatch, getState) => {
        var state = getState();
        var customerId = 1
        var url = window.getServerUrl();
        fetch(url + 'api/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                customerId: customerId,
                subject: values.subject,
                description: values.description
            })
        }).then(function (response) {
            return response.json();
        }).then(function (addInteractionResponse) {
            //alert(JSON.stringify(addInteractionResponse))
            if (addInteractionResponse.status == 512) {
                if (addInteractionResponse.message.match(/customer does not exists with provided customerId.*/)) {
                    notify.createShowQueue()("success", "Customer does not exist");
                } else {
                    notify.createShowQueue()("success", "Ticket cannot be created");
                }
            } else if(addInteractionResponse.status == 200){
                notify.createShowQueue()("success", "Ticket Raised Successfully");
            }
            dispatch(getAllInteration());
        });
    }
}


export function getAllInteration(customer_id = null) {
    return (dispatch, getState) => {
        let interactionData = "";
        var state = getState();
        var customerId;
        if (customer_id === null) {
            customerId = state.login.loginStatus.message.customer_id;
        } else {
            customerId = customer_id
        }
        dispatch({ type: 'TICKET_DATA_MESSAGE', interactionMessage: 'Loading...' });
        var url = window.getServerUrl();
        fetch(url + 'api/getAllInteraction', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                customer_id: customerId,

            })
        })
            .then(function (response) {
                return response.json();
            }).then(function (interactionData) {
                var ticketMessage = interactionData.tickets.length > 0 ? '' : 'Great you don\'t have any ticket..'
                dispatch({
                    type: 'TICKET_DATA',
                    interactionData: interactionData.tickets
                });
                dispatch({
                    type: 'TICKET_DATA_MESSAGE',
                    interactionMessage: ticketMessage
                });
            });
    }
}

export function doSearch(values) {
    return (dispatch, getState) => {

        dispatch({
            type: 'FILTER_PARAM',
            data: values
        });
    }
}

export function setPageNumber(values) {
    return (dispatch, getState) => {
        dispatch({
            type: 'PAGE_NUMBER',
            pageNumber: values
        });
    }
}